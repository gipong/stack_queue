#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<conio.h>
#define N 256*256

#define width 256
#define height 256

using namespace std;
void pushbyq(int a);
int popbyq();
int emptybyq();

int q[N];
int front=-1,rear=-1;

void main()
{
	unsigned char *image;
	char input[20];
    int x,y;//seed的x,y值
    int thr=30;//門檻值
	int seed,seed_value;
//    int top=-1;
	int pos;
    FILE *fp,*fc;
	

	

//    stack<int> region;
	image=new unsigned char[width*height];

//-----------------------------讀檔----------------------------ok---------
	cout<<"請輸入檔名:"<<endl;
	cin>>input;
	fp=fopen(input,"rb");
	fread(image,sizeof(unsigned char),width*height,fp);

//-----------------------------region growing-------------------------------
	printf("seed : enter x,y \n");
	scanf("%d%d",&x,&y);

	seed=x*width+y;
	seed_value=image[seed];
	
    pushbyq(seed);
	image[seed]=255;

	while (!emptybyq())
	{
		popbyq();
		pos=q[front];
//		image[pos]=255;   
		
		
		
		//左
		if(pos-1>0){
        if (abs(image[pos-1]-seed_value)<thr && image[pos-1]!=255)
		{
			pushbyq(pos-1);
			image[pos-1]=255;
		}}
		//右
		if(pos+1>0){
        if (abs(image[pos+1]-seed_value)<thr && image[pos+1]!=255)
		{
			pushbyq(pos+1);
			image[pos+1]=255;
		}}
		//下
		if(pos-width>0){
        if (abs(image[pos-width]-seed_value)<thr && image[pos-width]!=255)
		{
			pushbyq(pos-width);
			image[pos-width]=255;
		}}
		//上
		if(pos+width>0){
        if (abs(image[pos+width]-seed_value)<thr && image[pos+width]!=255)
		{
			pushbyq(pos+width);
			image[pos+width]=255;
		}}
		

	}
	fc=fopen("test.raw","wb");
	fwrite(image,sizeof(unsigned char),width*height,fc);
	fclose(fp);
	fclose(fc);
	
}
void pushbyq(int a)
{

	if(rear==N-1){
		printf("the queue is full.\n");
		exit(1);
	}
	else
		q[++rear]=a;
	
}
int popbyq()
{
	if(front==rear){
		printf("the queue is empty.\n");
		exit(1);
	}
	else{

		return(q[++front]);
	}
}
int emptybyq()
{
	if(front==rear)
		return 1;
	else
		return 0;

}