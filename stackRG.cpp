#include <stdio.h>
#include <stdlib.h>
#include<iostream>
#define N 256*256

#define width 256
#define height 256

using namespace std;

int thestack[N];
void thepush(int a);
void thepop();
int theEmpty();
int top=-1;
void main()
{
	unsigned char *image;
	char input[20];
    int x,y;//seed的x,y值
    int thr=30;//門檻值
	int seed,seed_value;
    
	int pos;
    FILE *fp,*fc;

//    stack<int> region;
	image=new unsigned char[width*height];

//-----------------------------讀檔----------------------------ok---------
	cout<<"請輸入檔名:"<<endl;
	cin>>input;
	fp=fopen(input,"rb");
	fread(image,sizeof(unsigned char),width*height,fp);

//-----------------------------region growing-------------------------------
	printf("seed : enter x and y \n");
	scanf("%d%d",&x,&y);

	seed=x*width+y;
	seed_value=image[seed];
	
    thepush(seed);
    image[seed]=255;

	while (!theEmpty())//region.empty())
	{
		pos=thestack[top];//region.top();
		image[pos]=255;
		thepop();
		//加入判斷pos的處理在遇到邊界時停止編譯
		//左
        if (pos>=1&&abs(image[pos-1]-seed_value)<thr && image[pos-1]!=255)
			thepush(pos-1);
//			region.push(pos-1);
		//右
        if (pos>=1&&abs(image[pos+1]-seed_value)<thr && image[pos+1]!=255)
			thepush(pos+1);
//			region.push(pos+1);
		//下
        if (pos>=1&&abs(image[pos-width]-seed_value)<thr && image[pos-width]!=255)
			thepush(pos-width);
//			region.push(pos-width);
		//上
        if (pos>=1&&abs(image[pos+width]-seed_value)<thr && image[pos+width]!=255)
			thepush(pos+width);
//			region.push(pos+width);
		
	}
	fc=fopen("test.raw","wb");
	fwrite(image,sizeof(unsigned char),width*height,fc);
	fclose(fp);
	fclose(fc);

}
void thepush(int a)
{
	if(top==N-1){
		printf("the stack is full\n");
		exit(1);
	}
	thestack[++top]=a;
}
void thepop()
{
	if(top==-1){
		printf("the stack is empty\n");
		exit(1);
	}
	thestack[top--];
}
int theEmpty()
{
	if(top==-1)
		return -1;
	else
		return 0;
}
